#pragma once
#include <graphics.h>
namespace JComps {
	enum JComponentType//�������
	{
		BASE = 0,
		LABEL = 1,
		BUTTON = 2,
		TEXTLINE = 3,
		SLOT = 4,
	};
	class JComponentBase//���������
	{
	public:
		enum BackgroundType
		{
			SOLIDCOLOR = 0,//��ɫ���
			IMAGEFILL = 1//ͼƬ���
		};
	public:
		JComponentBase(int x, int y, int width, int height);
		~JComponentBase();
		virtual JComponentType GetType() { return BASE; };
		void SetObjectName(const char* name);
		bool isObjectNameSame(const char* name);
		void Show();
		void SetBKState(bool state);
		void SetFlatState(bool state);
		void SetBKColor(COLORREF color);
		void SetFlatColor(COLORREF color);
		void SetFillType(BackgroundType type);
		void SetImg(const char* path);
		int GetX() { return x; };
		int GetX1() { return x; };
		int GetX2() { return x + width; };
		int GetLeft() { return x; };
		int GetRight() { return x + width; };
		int GetY() { return y; };
		int GetY1() { return y; };
		int GetY2() { return y + height; };
		int GetTop() { return y; };
		int GetBottom() { return y + height; };
		int GetWidth() { return width; };
		int GetHeight() { return height; };
		void SetCoverState(bool);
		bool GetCoverState();
	protected:
		void initComponent();//��ʼ�����
		virtual void Draw();
		void StartDraw();
		void EndDraw();
	protected:
		virtual void DrawBackground();
	protected:
		float x, y, width, height;
		bool isbkFill;//�Ƿ񱳾����
		BackgroundType filltype;//�����������
		bool isFlat;//�Ƿ��б߿�
		COLORREF fillcolor;//���ɫ
		COLORREF flatcolor;//�߿�ɫ
		IMAGE* img;//����
		IMAGE selfImg;
		HRGN hrgn;
		char* ObjectName;
		bool isCover;
	};
}

