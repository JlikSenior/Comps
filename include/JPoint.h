#pragma once
namespace JComps {
	class JPoint
	{
	public:
		JPoint();
		JPoint(int x, int y);
		void Setx(int x);
		int Getx();
		void Sety(int y);
		int Gety();
	private:
		int x;
		int y;
	};
}

