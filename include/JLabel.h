#pragma once
#include "JComponentBase.h"
namespace JComps
{
    class JLabel :
        public JComponentBase
    {
    public:
        JLabel(int x, int y, int width, int height, const char* text);
        JComponentType GetType() { return LABEL; };
        void SetTextColor(COLORREF color);
        void SetFontSize(int size);
        void SetFontStyle(const char* style);
        void SetText(const char* text);
    protected:
        void Draw() override;
    protected:
        char* text;
        COLORREF textcolor;
        LOGFONT font;
    };
}


