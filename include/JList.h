#pragma once
#include <stdio.h>
namespace JComps
{
	class JNode
	{
	public:
		JNode();
		JNode(void* data, int ord);
		~JNode() {
			delete m_data;
			m_data = NULL;
			next = NULL;
		};
		void SetOrd(int ord);
		void SetData(void* data);
		int GetOrd() { return ord; };
		void SetNextNode(JNode* next);
		JNode* GetNextNode() { return next; };
		void SetPreNode(JNode* pre);
		JNode* GetPreNode();
		void CutNextNode() { if (next) { next->preNode = NULL; next = NULL; } };
		void* Data() { return m_data; };
		bool isValid();
		void CopyData(JNode other);
		void UpdataOrd();
	private:
		void* m_data;
		int ord;
		JNode* next;
		JNode* preNode;
	};
	class JList
	{
	public:
		JList();
		void* data(int ord);
		JNode* NodeData(int ord);
		void push_back(void* data);
		JNode* pop_back();
		void push_front(void* data);
		JNode* pop_front();
		JNode* back();
		JNode* front();
		void insert(int ord, void* data);
		JNode* remove(int ord);
		void clear();
		int size();
		int lenght();
		bool swap(int i, int j);
		~JList();
	private:
		JNode* tail;
		JNode* list;
		int m_size;
	};
}


