#pragma once
#include "JButton.h"
#include <time.h>
namespace JComps
{
	class JTextLine :public JButton
	{
	public:
		JTextLine(int x, int y, int width, int heigth, COLORREF in, COLORREF out);
		JComponentType GetType() { return TEXTLINE; };
		char* GetText();
		void SetPlaintextState(bool state);
	protected:
		void Draw() override;
		bool CheckEditState();
		void Input();
	protected:
		bool isEdit;
		bool ishow;
		bool isPlaintext;
		bool ishide;
		char content[1025] = { 0 };
		int count;
		clock_t time_t;
	};
}


