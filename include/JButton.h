#pragma once
#include "JLabel.h"
#include "JTimer.h"
namespace JComps
{
    class JButton :
        public JLabel
    {
    public:
        JButton(int x, int y, int width, int height, const char* text, COLORREF in, COLORREF out);
        JComponentType GetType() { return BUTTON; };
        bool cilck();
    protected:
        virtual bool hover();
        virtual bool isClick();
        void Draw() override;
    protected:
        COLORREF incolor;
        COLORREF outcolor;
        bool isHover;
        bool isPress;
        static ExMessage m;
        bool clicked;
        static JTimer g_timer;
    };
}


