#pragma once
#include <time.h>
namespace JComps
{
	class JTimer
	{
	public:
		JTimer();
		JTimer(int msc);
		void Start(int msc);
		void SetEnable(bool state);
		bool GetEnable();
		bool TimeOut();
	private:
		clock_t start;
		int delay;
		int isStart;
	};
}


